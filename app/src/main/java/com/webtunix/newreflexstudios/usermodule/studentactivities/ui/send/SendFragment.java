package com.webtunix.newreflexstudios.usermodule.studentactivities.ui.send;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.webtunix.newreflexstudios.R;

public class SendFragment extends Fragment {



    private SendViewModel sendViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE,
                WindowManager.LayoutParams.FLAG_SECURE);
        sendViewModel =
                ViewModelProviders.of(this).get(SendViewModel.class);
        View root = inflater.inflate(R.layout.fragment_send, container, false);


        TextView textView = root.findViewById(R.id.text_send);



        textView.setText("We have the capacity to Create, Store and Distribute recordings for all classes from Primary, Secondary\n" +
                "TVET and College Courses for all Levels \"Social Distancing\" has its unintended consequences in all our\n" +
                "interactions. NEW REFLEX STUDIO EDUCATIONAL RESOURCE CHANNEL has what we think is a suitable\n" +
                "system that can work in the interim period and beyond as we move toward our \"NEW NORMAL\".");

            return root;
    }


}
