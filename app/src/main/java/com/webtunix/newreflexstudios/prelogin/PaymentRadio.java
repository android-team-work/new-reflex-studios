package com.webtunix.newreflexstudios.prelogin;

public class PaymentRadio {

    private Boolean value;

    PaymentRadio(){

    }

    public Boolean getValue() {
        return value;
    }


    public void setValue(Boolean value) {
        this.value = value;
    }
}
